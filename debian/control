Source: coolprop
Section: science
Priority: optional
Maintainer: Kurt Kremitzki <kurt@kwk.systems>
Build-Depends: catch,
               cmake,
               cython,
               cython3,
               debhelper (>=11~),
               dh-python,
               doxygen,
               libeigen3-dev,
               libfmt-dev,
               libif97-dev,
               libmsgpack-dev,
               pybind11-dev,
               python-dev,
               python-pkg-resources,
               python-setuptools,
               python3-dev,
               python3-pkg-resources,
               python3-setuptools,
               rapidjson-dev
Standards-Version: 4.1.5
Vcs-Browser: https://salsa.debian.org/science-team/coolprop
Vcs-Git: https://salsa.debian.org/science-team/coolprop.git
Homepage: https://github.com/coolprop/coolprop

Package: libcoolprop-6.1
Architecture: any
Section: libs
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: Fluid dynamics and engineering design library - library
 Fluids is open-source software for engineers and technicians working in
 the fields of chemical, mechanical, or civil engineering. It includes
 modules for piping, fittings, pumps, tanks, compressible flow,
 open-channel flow, and more.
 .
 The fluids library depends on the SciPy library to provide numerical
 constants, interpolation, integration, and numerical solving functionality.
 .
 This package contains Coolprop shared library.

Package: libcoolprop-dev
Architecture: any
Section: libdevel
Depends: ${misc:Depends},
         libcoolprop-6.1 (<< ${binary:Version}+1~),
         libcoolprop-6.1 (>= ${binary:Version}),
Description: Fluid dynamics and engineering design library - headers
 Fluids is open-source software for engineers and technicians working in
 the fields of chemical, mechanical, or civil engineering. It includes
 modules for piping, fittings, pumps, tanks, compressible flow,
 open-channel flow, and more.
 .
 The fluids library depends on the SciPy library to provide numerical
 constants, interpolation, integration, and numerical solving functionality.
 .
 This package contains Coolprop headers.

Package: python-coolprop
Architecture: any
Section: python
Depends: ${misc:Depends}, ${python:Depends}, ${shlibs:Depends}
Suggests: python-coolprop-doc
Provides: ${python:Provides}
Description: Python fluid dynamics and engineering design library
 Fluids is open-source software for engineers and technicians working in
 the fields of chemical, mechanical, or civil engineering. It includes
 modules for piping, fittings, pumps, tanks, compressible flow,
 open-channel flow, and more.
 .
 The fluids library depends on the SciPy library to provide numerical
 constants, interpolation, integration, and numerical solving functionality.
 .
 This package contains Python coolprop.

Package: python3-coolprop
Architecture: any
Section: python
Depends: ${misc:Depends}, ${python3:Depends}, ${shlibs:Depends}
Suggests: python-coolprop-doc
Provides: ${python3:Provides}
Description: Python 3 fluid dynamics and engineering design library
 Fluids is open-source software for engineers and technicians working in
 the fields of chemical, mechanical, or civil engineering. It includes
 modules for piping, fittings, pumps, tanks, compressible flow,
 open-channel flow, and more.
 .
 The fluids library depends on the SciPy library to provide numerical
 constants, interpolation, integration, and numerical solving functionality.
 .
 This package contains Python 3 coolprop.

Package: libcoolprop-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}
Description: Fluid dynamics and engineering design library - docs
 Fluids is open-source software for engineers and technicians working in
 the fields of chemical, mechanical, or civil engineering. It includes
 modules for piping, fittings, pumps, tanks, compressible flow,
 open-channel flow, and more.
 .
 The fluids library depends on the SciPy library to provide numerical
 constants, interpolation, integration, and numerical solving functionality.
 .
 This package contains the documentation files.
